from pathlib import Path
from typing import List
from dataclasses import dataclass


@dataclass
class Instruction:
   amount: int
   from_idx: int
   to_idx: int


INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")

#
# Parse inputs
#
stacks: List[List[str]] = []
instructions: List[Instruction] = []

with INPUT_FILE.open("r") as f:
   lines = [l.rstrip() for l in f.readlines()]
   instruction_start = None

   # Parse stacks
   for line_idx, l in enumerate(lines):
      if l == "":
         instruction_start = line_idx + 1
         break
      for i, crate in enumerate(l.rstrip()[1::4]):
         if len(stacks) <= i:
            stacks.append([])
         if crate.strip() != "":
            stacks[i].append(crate)

   # Parse instructions
   for l in lines[instruction_start:]:
      _, amt, _, from_idx, _, to_idx = l.split(" ")
      instructions.append(Instruction(int(amt), int(from_idx) - 1, int(to_idx) - 1))

stacks = [list(reversed(s[:-1])) for s in stacks]

#
# Execute instructions
#
for instr in instructions:
   # Remove from source stack
   temp = stacks[instr.from_idx][-instr.amount:]
   stacks[instr.from_idx] = stacks[instr.from_idx][:-instr.amount]

   # Add to destination stack
   stacks[instr.to_idx] += list(temp)

print("".join(map(lambda s: s[-1], stacks)))
