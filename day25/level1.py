from pathlib import Path
from dataclasses import dataclass
from typing import Union, Callable, Dict, List, Tuple, Set
from enum import Enum, auto

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")


def from_snafu(snafu: str) -> int:
    VALUES = {
        "2": 2,
        "1": 1,
        "0": 0,
        "-": -1,
        "=": -2,
    }
    result = 0
    for idx, c in enumerate(reversed(snafu)):
        result += 5**idx * VALUES[c]
    return result


def sign(num: int) -> int:
    if num > 0:
        return 1
    elif num < 0:
        return -1
    else:
        return 0


def to_snafu(num: int) -> str:
    VALUES = {
        2: "2",
        1: "1",
        0: "0",
        -1: "-",
        -2: "=",
    }

    curr_snafu: List[str] = []
    curr_snafu_num = 0

    curr_place = 1
    while 3 * curr_place <= num:
        curr_place *= 5

    while curr_place > 0:
        curr_place_amt = 0

        curr_place_amt = 0
        delta = curr_place
        while abs(num - (curr_snafu_num + curr_place*curr_place_amt)) > curr_place / 2:
            delta = num - (curr_snafu_num + curr_place*curr_place_amt)
            curr_place_amt += sign(delta)

        curr_snafu.append(VALUES[curr_place_amt])
        curr_snafu_num += curr_place_amt * curr_place
        curr_place //= 5

    return "".join(curr_snafu)


with INPUT_FILE.open("r") as f:
    lines = f.readlines()
    print(to_snafu(sum(from_snafu(l.strip()) for l in lines)))
