from pathlib import Path
from dataclasses import dataclass
from typing import Union, Callable, Dict

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")

monkey_map: Dict[str, Union["MonkeyOp", "MonkeyValue"]] = {}


@dataclass
class MonkeyValue:
    name: str
    value: int

    def get_value(self) -> int:
        return self.value


@dataclass
class MonkeyOp:
    name: str
    monkey1: str
    monkey2: str
    op: Callable[[int, int], int]

    def get_value(self) -> int:
        v1 = monkey_map[self.monkey1].get_value()
        v2 = monkey_map[self.monkey2].get_value()
        return self.op(v1, v2)


OPERATIONS: Dict[str, Callable[[int, int], int]] = {
    "*": lambda v1, v2: v1 * v2,
    "+": lambda v1, v2: v1 + v2,
    "-": lambda v1, v2: v1 - v2,
    "/": lambda v1, v2: v1 // v2,
}

with INPUT_FILE.open("r") as f:
    for l in f.readlines():
        name, val = l.strip().split(":")

        try:
            m = MonkeyValue(name, int(val.strip()))
        except ValueError:
            lhs, op, rhs = val.strip().split(" ")
            m = MonkeyOp(name, lhs, rhs, OPERATIONS[op])

        monkey_map[name] = m

print(monkey_map["root"].get_value())
