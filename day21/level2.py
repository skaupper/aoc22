from pathlib import Path
from dataclasses import dataclass
from typing import Union, Callable, Dict

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")

monkey_map: Dict[str, Union["MonkeyOp", "MonkeyValue"]] = {}


@dataclass
class MonkeyValue:
    name: str
    value: int

    def get_value(self) -> int:
        return self.value

    def has_human(self) -> bool:
        return self.name == "humn"


@dataclass
class MonkeyOp:
    name: str
    monkey1: str
    monkey2: str
    op: Callable[[int, int], int]

    def get_value(self) -> int:
        v1 = monkey_map[self.monkey1].get_value()
        v2 = monkey_map[self.monkey2].get_value()
        return self.op(v1, v2)

    def has_human(self) -> bool:
        if self.name == "humn":
            return True
        return monkey_map[self.monkey1].has_human() or monkey_map[self.monkey2].has_human()


OPERATIONS: Dict[str, Callable[[int, int], int]] = {
    "*": lambda v1, v2: v1 * v2,
    "+": lambda v1, v2: v1 + v2,
    "-": lambda v1, v2: v1 - v2,
    "/": lambda v1, v2: v1 // v2,
}

with INPUT_FILE.open("r") as f:
    for l in f.readlines():
        name, val = l.strip().split(":")

        try:
            m = MonkeyValue(name, int(val.strip()))
        except ValueError:
            lhs, op, rhs = val.strip().split(" ")
            m = MonkeyOp(name, lhs, rhs, OPERATIONS[op])

        monkey_map[name] = m

goal_value = 0
human_root = ""

if monkey_map[monkey_map["root"].monkey1].has_human():
    goal_value = monkey_map[monkey_map["root"].monkey2].get_value()
    # human_root = monkey_map["root"].monkey2
    human_root = monkey_map["root"].monkey1
elif monkey_map[monkey_map["root"].monkey2].has_human():
    goal_value = monkey_map[monkey_map["root"].monkey1].get_value()
    # human_root = monkey_map["root"].monkey1
    human_root = monkey_map["root"].monkey2

print(goal_value)
for i in range(3305669000000, 3305670000000):
    monkey_map["humn"].value = i
    if goal_value == monkey_map[human_root].get_value():
        print(f"{goal_value} == {monkey_map[human_root].get_value()}")
        print(i)
        break

# i = 0
# while monkey_map[human_root].get_value() != goal_value:
#     monkey_map["humn"].value = i
#     if i % 10000 == 0:
#         print(i)
#     i += 1
# print(i)
