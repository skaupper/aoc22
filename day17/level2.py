from pathlib import Path
from dataclasses import dataclass
from typing import Union, Callable, Dict, List, Tuple, Set, Optional
from enum import Enum, auto

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")
# INPUT_FILE = Path("example-2.txt")


@dataclass(frozen=True)
class Vector:
    x: int
    y: int

    def __mul__(self, f: int) -> "Vector":
        return Vector(self.x * f, self.y * f)


@dataclass(frozen=True)
class Point:
    x: int
    y: int

    def __add__(self, dir: Vector) -> "Point":
        return Point(self.x + dir.x, self.y + dir.y)


# Shape coordinates start from the lower left corner and count to the left and upwards
SHAPES = [
    set([Vector(0, 0), Vector(1, 0), Vector(2, 0), Vector(3, 0)]),  # Horizontal bar
    set([Vector(1, 0), Vector(0, 1), Vector(1, 1), Vector(2, 1),
         Vector(1, 2)]),  # Cross
    set([Vector(2, 0), Vector(2, 1), Vector(2, 2), Vector(1, 0),
         Vector(0, 0)]),  # L
    set([Vector(0, 0), Vector(0, 1), Vector(0, 2), Vector(0, 3)]),  # Vertical bar
    set([Vector(0, 0), Vector(0, 1), Vector(1, 0), Vector(1, 1)]),  # Box
]

with INPUT_FILE.open("r") as f:
    directions_raw = f.read().strip()
    directions = [Vector(1, 0) if d == ">" else Vector(-1, 0) for d in directions_raw]

MAX_WIDTH = 7
START_OFFSET = Vector(2, 3)


def print_map(settled_rocks: Set[Point], current_shape: Optional[Set[Point]] = None) -> None:
    if current_shape is None:
        current_shape = set()

    height = max(p.y for p in settled_rocks) if len(settled_rocks) > 0 else 0

    for y in range(height + 3, -1, -1):
        for x in range(-1, MAX_WIDTH + 1):
            if x < 0 or x >= MAX_WIDTH:
                print("|", end="")
                continue

            if Point(x, y) in settled_rocks:
                print("#", end="")
            elif Point(x, y) in current_shape:
                print("@", end="")
            else:
                print(".", end="")
        print()
    print(f"+{'-'*MAX_WIDTH}+")
    print()


def move_if_possible(current_shape: Set[Point], dir: Vector, settled_rocks: Set[Point]) -> Optional[Set[Point]]:
    moved_shape = {p + dir for p in current_shape}

    crashed_boundary = min(p.x for p in moved_shape) < 0 or max(p.x for p in moved_shape) >= MAX_WIDTH
    if crashed_boundary:
        moved_shape = current_shape

    crashed_rocks = len(moved_shape.intersection(settled_rocks)) > 0 or min(p.y for p in moved_shape) < 0
    if crashed_rocks:
        return None

    return moved_shape


def step(current_shape: Set[Point], dir: Vector, settled_rocks: Set[Point]) -> Optional[Set[Point]]:
    blown_shape = move_if_possible(current_shape, dir, settled_rocks)
    if blown_shape is None:
        blown_shape = current_shape

    sunk_shape = move_if_possible(blown_shape, Vector(0, -1), settled_rocks)
    if sunk_shape is None:
        settled_rocks.update(blown_shape)

    return sunk_shape


def step_shape(shape_idx: int, dir_idx: int, settled_rocks: Set[Point]) -> int:
    height = max([0, *[p.y + 1 for p in settled_rocks]])
    current_shape = {Point(0, height) + START_OFFSET + v for v in SHAPES[shape_idx]}

    while current_shape is not None:
        current_shape = step(current_shape, directions[dir_idx], settled_rocks)
        # print_map(settled_rocks, current_shape)
        dir_idx = (dir_idx+1) % len(directions)

    return dir_idx


dir_idx = 0
shape_idx = 0
settled_rocks = set()

last_height = 0
for i in range(100000):
    dir_idx = step_shape(shape_idx, dir_idx, settled_rocks)
    height = max([0, *[p.y + 1 for p in settled_rocks]])

    # It is possible to detect loops in height differences.
    # Since I am lazy, I did it by hand :)
    print(f"{i:4}: height={height:>6}; diff={last_height-height:>4}")

    shape_idx = (shape_idx+1) % len(SHAPES)
    last_height = height

print(max(p.y for p in settled_rocks) + 1)
