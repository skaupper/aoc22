from pathlib import Path
from typing import Tuple, List, Iterator, Callable, Union, Literal, Dict
import re
from dataclasses import dataclass
import functools

INPUT_FILE = Path("input.txt")
INPUT_FILE = Path("example.txt")


LINE_0_PATTERN = re.compile(r"^Monkey (\d+):$")
LINE_1_PATTERN = re.compile(r"^Starting items: (\d+(?:, \d+)*)$")
LINE_2_PATTERN = re.compile(r"^Operation: new = old ([+*]) (\d+|old)$")
LINE_3_PATTERN = re.compile(r"^Test: divisible by (\d+)$")
LINE_4_PATTERN = re.compile(r"^If true: throw to monkey (\d+)$")
LINE_5_PATTERN = re.compile(r"^If false: throw to monkey (\d+)$")

@dataclass
class Monkey:
    _nr: int
    _items: List[int]
    _op_cmd: Tuple[Callable[[int, int], int], Union[int, Literal["old"]]]
    _test_nr: int
    _test_true_monkey: int
    _test_false_monkey: int

    _mod_div: int = 1
    _items_inspected: int = 0

    @classmethod
    def from_lines(cls, lines: Iterator[str]) -> "Monkey":
        l = next(lines)
        while len(l) == 0:
            l = next(lines)

        m = LINE_0_PATTERN.match(l)
        assert m is not None
        inst_nr = int(m.group(1))

        m = LINE_1_PATTERN.match(next(lines))
        assert m is not None
        inst_items = [int(item.strip()) for item in m.group(1).split(",")]

        m = LINE_2_PATTERN.match(next(lines))
        assert m is not None
        match m.group(1):
            case "*": _op = lambda e1,e2: e1*e2
            case "+": _op = lambda e1,e2: e1+e2
            case _: assert False
        _operand = m.group(2)
        if _operand != "old":
            _operand = int(_operand)
        inst_op_cmd = (_op, _operand)


        m = LINE_3_PATTERN.match(next(lines))
        assert m is not None
        inst_test_nr = int(m.group(1))

        m = LINE_4_PATTERN.match(next(lines))
        assert m is not None
        inst_test_true_monkey = int(m.group(1))

        m = LINE_5_PATTERN.match(next(lines))
        assert m is not None
        inst_test_false_monkey = int(m.group(1))

        return cls(inst_nr, inst_items, inst_op_cmd, inst_test_nr,
                   inst_test_true_monkey, inst_test_false_monkey)

    def add_item(self, item: int) -> None:
        self._items.append(item)

    def get_nr(self) -> int:
        return self._nr

    def get_items_inspected(self) -> int:
        return self._items_inspected

    def _inspect(self, item: int) -> int:
        self._items_inspected += 1
        operand = self._op_cmd[1]
        if operand == "old":
            operand = item
        item = self._op_cmd[0](item, operand)
        item = item % self._mod_div
        return item

    def _test_item(self, item: int) -> bool:
        return item % self._test_nr == 0

    def get_test_nr(self) -> int:
        return self._test_nr

    def set_item_divisor(self, div: int) -> None:
        self._mod_div = div

    def do_turn(self, all_monkeys: Dict[int, "Monkey"]) -> None:
        for item in self._items:
            item = self._inspect(item)

            if self._test_item(item):
                all_monkeys[self._test_true_monkey].add_item(item)
            else:
                all_monkeys[self._test_false_monkey].add_item(item)
        self._items = []





monkeys_list = []

with INPUT_FILE.open("r") as f:
    lines = (l.strip() for l in f.readlines())
    try:
        while True:
            monkeys_list.append(Monkey.from_lines(lines))
    except StopIteration:
        pass

monkeys_list = sorted(monkeys_list, key=lambda m: m.get_nr())
all_monkeys = {m.get_nr(): m for m in monkeys_list}
mod_div = functools.reduce(lambda e1,e2: e1*e2, [m.get_test_nr() for m in monkeys_list], 1)

for m in monkeys_list:
    m.set_item_divisor(mod_div)


MAX_ROUNDS = 10000
for _ in range(MAX_ROUNDS):
    for m in monkeys_list:
        m.do_turn(all_monkeys)

monkeys_list = sorted(monkeys_list, key=lambda m: m.get_items_inspected(), reverse=True)

print(monkeys_list[0].get_items_inspected()*monkeys_list[1].get_items_inspected())
