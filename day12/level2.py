from pathlib import Path
from typing import Tuple, List, Iterator, Callable, Union, Literal, Dict, Optional
import re
from dataclasses import dataclass
from graphs import Edge, get_all_shortest_paths, PathFinderProtocol

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")


@dataclass(frozen=True, eq=True)
class Point:
    x: int
    y: int


elevation_map: Dict[Point, int] = {}
start_points: List[Point] = []
end_point: Optional[Point] = None

with INPUT_FILE.open("r") as f:
    for y, row in enumerate(f.readlines()):
        for x, col in enumerate(row.strip()):
            if col == "S":
                col = "a"
            elif col == "E":
                col = "z"
                end_point = Point(x, y)

            if col == "a":
                start_points.append(Point(x, y))

            elevation_map[Point(x, y)] = ord(col) - ord("a")

assert end_point is not None


class PathFinderInst(PathFinderProtocol[Point]):

    def __init__(self, end: Point, elevation_map: Dict[Point, int]) -> None:
        self._map = elevation_map
        self._end = end
        self._width = max(elevation_map.keys(), key=lambda p: p.x).x + 1
        self._height = max(elevation_map.keys(), key=lambda p: p.y).y + 1

    def get_neighbours(self, point: Point) -> List[Edge[Point]]:
        candidates = [
            Point(point.x - 1, point.y),
            Point(point.x + 1, point.y),
            Point(point.x, point.y - 1),
            Point(point.x, point.y + 1),
        ]
        filtered_candidates = [
            Edge(p, 1)  #
            for p in candidates
            if ((0 <= p.x < self._width) and (0 <= p.y < self._height)) and (self._map[point] - self._map[p]) <= 1
        ]
        return filtered_candidates


pf = PathFinderInst(end_point, elevation_map)
all_nodes = get_all_shortest_paths(end_point, pf)

all_low_nodes = {k: v[0] for k, v in all_nodes.items() if elevation_map[k] == 0}
print(min(all_low_nodes.values()))
