from pathlib import Path
from typing import Tuple, List, Iterator, Callable, Union, Literal, Dict, Optional
import re
from dataclasses import dataclass
from graphs import Edge, get_shortest_path

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")


@dataclass(frozen=True, eq=True)
class Point:
    x: int
    y: int


elevation_map: Dict[Point, int] = {}
start_point: Optional[Point] = None
end_point: Optional[Point] = None

with INPUT_FILE.open("r") as f:
    for y, row in enumerate(f.readlines()):
        for x, col in enumerate(row.strip()):
            if col == "S":
                col = "a"
                start_point = Point(x, y)
            elif col == "E":
                col = "z"
                end_point = Point(x, y)

            elevation_map[Point(x, y)] = ord(col) - ord("a")

assert start_point is not None
assert end_point is not None


class PathFinderInst:

    def __init__(self, start: Point, end: Point, elevation_map: Dict[Point, int]) -> None:
        self._map = elevation_map
        self._start = start
        self._end = end
        self._width = max(elevation_map.keys(), key=lambda p: p.x).x + 1
        self._height = max(elevation_map.keys(), key=lambda p: p.y).y + 1

    def get_neighbours(self, point: Point) -> List[Edge[Point]]:
        candidates = [
            Point(point.x - 1, point.y),
            Point(point.x + 1, point.y),
            Point(point.x, point.y - 1),
            Point(point.x, point.y + 1),
        ]
        filtered_candidates = [
            Edge(p)  #
            for p in candidates
            if ((0 <= p.x < self._width) and (0 <= p.y < self._height)) and (self._map[p] - self._map[point]) <= 1
        ]
        return filtered_candidates

    def get_distance_to_target(self, point: Point) -> float:
        import math
        return math.sqrt((self._end.x - point.x)**2 + (self._end.y - point.y)**2)

    def is_target(self, point: Point) -> bool:
        return self._end == point


pf = PathFinderInst(start_point, end_point, elevation_map)
path = get_shortest_path(start_point, pf)
assert path is not None

# print(path)
print(len(path) - 1)
