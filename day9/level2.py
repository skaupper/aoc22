from pathlib import Path
from typing import Tuple, List

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example2.txt")

SEGMENT_CNT = 10

segment_positions: List[Tuple[int, int]] = [(0, 0) for _ in range(SEGMENT_CNT)]
visited_positions = [set() for _ in range(SEGMENT_CNT)]
directions = []

with INPUT_FILE.open("r") as f:
    for l in f.readlines():
        dir, amt = l.split(" ")

        match dir:
            case "R": dir_vec = (1,0)
            case "L": dir_vec = (-1,0)
            case "U": dir_vec = (0,1)
            case "D": dir_vec = (0,-1)
            case _: assert False

        directions += [dir_vec]*int(amt)

def sign(i: int) -> int:
    if i == 0:
        return 0
    elif i > 0:
        return 1
    else:
        return -1

def follow(head: Tuple[int, int], tail: Tuple[int, int]) -> Tuple[int, int]:
    h_x, h_y = head
    t_x, t_y = tail
    d = max(abs(h_x-t_x), abs(h_y-t_y))

    if d < 2:
        return tail

    x = t_x + sign(h_x-t_x)
    y = t_y + sign(h_y-t_y)
    return (x,y)



for d in directions:
    head = segment_positions[0]
    segment_positions[0] = (head[0]+d[0], head[1]+d[1])

    for i in range(1, len(segment_positions)):
        segment_positions[i] = follow(segment_positions[i-1], segment_positions[i])
        visited_positions[i].add(segment_positions[i])

print(len(visited_positions[-1]))
# print(visited_positions[1])
