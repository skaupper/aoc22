from pathlib import Path
from dataclasses import dataclass
from typing import Union, Callable, Dict, List, Tuple
from enum import Enum, auto


class Direction(Enum):
    RIGHT = "R"
    LEFT = "L"
    UP = "U"
    DOWN = "D"


INPUT_FILE = Path("input.txt")
CUBE_SIDE_LENGTH = 50
SECTION_MAPPINGS: Dict[Tuple[int, Direction], Tuple[int, Direction]] = {
    (0, Direction.UP): (5, Direction.LEFT),
    (0, Direction.RIGHT): (1, Direction.LEFT),
    (0, Direction.DOWN): (2, Direction.UP),
    (0, Direction.LEFT): (3, Direction.LEFT),
    #
    (1, Direction.UP): (5, Direction.DOWN),
    (1, Direction.RIGHT): (4, Direction.RIGHT),
    (1, Direction.DOWN): (2, Direction.RIGHT),
    (1, Direction.LEFT): (0, Direction.RIGHT),
    #
    (2, Direction.UP): (0, Direction.DOWN),
    (2, Direction.RIGHT): (1, Direction.DOWN),
    (2, Direction.DOWN): (4, Direction.UP),
    (2, Direction.LEFT): (3, Direction.UP),
    #
    (3, Direction.UP): (2, Direction.LEFT),
    (3, Direction.RIGHT): (4, Direction.LEFT),
    (3, Direction.DOWN): (5, Direction.UP),
    (3, Direction.LEFT): (0, Direction.LEFT),
    #
    (4, Direction.UP): (2, Direction.DOWN),
    (4, Direction.RIGHT): (1, Direction.RIGHT),
    (4, Direction.DOWN): (5, Direction.RIGHT),
    (4, Direction.LEFT): (3, Direction.RIGHT),
    #
    (5, Direction.UP): (3, Direction.DOWN),
    (5, Direction.RIGHT): (4, Direction.DOWN),
    (5, Direction.DOWN): (1, Direction.UP),
    (5, Direction.LEFT): (0, Direction.UP),
}

# INPUT_FILE = Path("example.txt")
# CUBE_SIDE_LENGTH = 4
# SECTION_MAPPINGS: Dict[Tuple[int, Direction], Tuple[int, Direction]] = {
#     (0, Direction.UP): (1, Direction.UP),
#     (0, Direction.RIGHT): (5, Direction.RIGHT),
#     (0, Direction.DOWN): (3, Direction.UP),
#     (0, Direction.LEFT): (2, Direction.UP),
#     #
#     (1, Direction.UP): (0, Direction.UP),
#     (1, Direction.RIGHT): (2, Direction.LEFT),
#     (1, Direction.DOWN): (4, Direction.DOWN),
#     (1, Direction.LEFT): (5, Direction.DOWN),
#     #
#     (2, Direction.UP): (0, Direction.LEFT),
#     (2, Direction.RIGHT): (3, Direction.LEFT),
#     (2, Direction.DOWN): (4, Direction.LEFT),
#     (2, Direction.LEFT): (1, Direction.RIGHT),
#     #
#     (3, Direction.UP): (0, Direction.DOWN),
#     (3, Direction.RIGHT): (5, Direction.UP),
#     (3, Direction.DOWN): (4, Direction.UP),
#     (3, Direction.LEFT): (2, Direction.RIGHT),
#     #
#     (4, Direction.UP): (3, Direction.DOWN),
#     (4, Direction.RIGHT): (5, Direction.LEFT),
#     (4, Direction.DOWN): (1, Direction.DOWN),
#     (4, Direction.LEFT): (2, Direction.DOWN),
#     #
#     (5, Direction.UP): (3, Direction.RIGHT),
#     (5, Direction.RIGHT): (0, Direction.RIGHT),
#     (5, Direction.DOWN): (1, Direction.LEFT),
#     (5, Direction.LEFT): (4, Direction.RIGHT),
# }


@dataclass(frozen=True)
class Vector:
    x: int
    y: int

    def rotate_clockwise(self) -> "Vector":
        return Vector(-self.y, self.x)

    def rotate_counter_clockwise(self) -> "Vector":
        return Vector(self.y, -self.x)

    def rotate(self, dir: Direction) -> "Vector":
        if dir == Direction.LEFT:
            return self.rotate_counter_clockwise()
        elif dir == Direction.RIGHT:
            return self.rotate_clockwise()
        assert False

    def to_direction(self) -> Direction:
        assert abs(self.x) + abs(self.y) == 1
        if self.x > 0:
            return Direction.RIGHT
        elif self.x < 0:
            return Direction.LEFT
        elif self.y > 0:
            return Direction.DOWN
        elif self.y < 0:
            return Direction.UP
        assert False


@dataclass(frozen=True)
class Point:
    x: int
    y: int

    def __add__(self, dir: Vector) -> "Point":
        return Point(self.x + dir.x, self.y + dir.y)


@dataclass(frozen=True)
class Rectangle:
    left_top: Point
    right_bottom: Point

    def __contains__(self, p: Point) -> bool:
        if (self.left_top.x <= p.x <= self.right_bottom.x) and (self.left_top.y <= p.y <= self.right_bottom.y):
            return True
        return False


class Tile(Enum):
    ROCK = "#"
    OPEN = "."


cave_map: Dict[Point, Tile] = {}
instructions: List[Union[int, Direction]] = []

with INPUT_FILE.open("r") as f:
    lines = iter(f.readlines())
    for y, l in enumerate(lines):
        if len(l.strip()) == 0:
            break

        for x, c in enumerate(l.rstrip()):
            try:
                cave_map[Point(x, y)] = Tile(c)
            except ValueError:
                pass

    num_str = []
    for c in next(lines).strip():
        try:
            dir = Direction(c)
            instructions.append(int("".join(num_str)))
            instructions.append(dir)
            num_str = []
        except ValueError:
            num_str.append(c)
    if len(num_str) > 0:
        instructions.append(int("".join(num_str)))

max_x = max(p.x for p in cave_map.keys())
max_y = max(p.y for p in cave_map.keys())

cave_row_bounds: List[Tuple[int, int]] = [
    (
        min(p.x for p in cave_map.keys() if p.y == y),  #
        max(p.x for p in cave_map.keys() if p.y == y)
    ) for y in range(max_y + 1)
]
cave_col_bounds: List[Tuple[int, int]] = [
    (
        min(p.y for p in cave_map.keys() if p.x == x),  #
        max(p.y for p in cave_map.keys() if p.x == x)
    ) for x in range(max_x + 1)
]

###############################################################################

cube_sections: List[Rectangle] = []

curr_min_x = 0
curr_y = 0

while curr_y < max_y:
    left_top = Point(min(p.x for p in cave_map.keys() if p.y == curr_y and p.x >= curr_min_x), curr_y)
    right_bottom = Point(left_top.x + CUBE_SIDE_LENGTH - 1, left_top.y + CUBE_SIDE_LENGTH - 1)
    cube_sections.append(Rectangle(left_top, right_bottom))

    curr_min_x = right_bottom.x + 1
    if curr_min_x >= max_x or not any(p.x for p in cave_map.keys() if p.y == curr_y and p.x >= curr_min_x):
        curr_min_x = 0
        curr_y += CUBE_SIDE_LENGTH


def get_section(p: Point) -> int:
    return next((i for i, s in enumerate(cube_sections) if p in s), None)


def get_entry_point_by_idx(section_idx: int, point_idx: int, entry_dir: Direction) -> Point:
    section = cube_sections[section_idx]

    if entry_dir == Direction.UP:
        x = section.left_top.x + point_idx
        y = section.left_top.y
    elif entry_dir == Direction.RIGHT:
        x = section.right_bottom.x
        y = section.left_top.y + point_idx
    elif entry_dir == Direction.DOWN:
        x = section.left_top.x + point_idx
        y = section.right_bottom.y
    elif entry_dir == Direction.LEFT:
        x = section.left_top.x
        y = section.left_top.y + point_idx
    else:
        assert False

    return Point(x, y)


def get_entry_point(p: Point, dir: Vector) -> Point:
    section_idx = get_section(p)
    next_section_idx, next_dir = SECTION_MAPPINGS[(section_idx, dir.to_direction())]

    leave_idx = 0
    if dir.x != 0:
        leave_idx = p.y - cube_sections[section_idx].left_top.y
    elif dir.y != 0:
        leave_idx = p.x - cube_sections[section_idx].left_top.x
    else:
        assert False

    invert_idx = False
    if dir.to_direction() in [Direction.UP, Direction.RIGHT] and next_dir in [Direction.UP, Direction.RIGHT]:
        invert_idx = True
    elif dir.to_direction() in [Direction.DOWN, Direction.LEFT] and next_dir in [Direction.DOWN, Direction.LEFT]:
        invert_idx = True

    entry_point_idx = leave_idx
    if invert_idx:
        entry_point_idx = CUBE_SIDE_LENGTH - entry_point_idx - 1
    return get_entry_point_by_idx(next_section_idx, entry_point_idx, next_dir)


def get_entry_dir(p: Point, dir: Vector) -> Vector:
    _, next_dir = SECTION_MAPPINGS[(get_section(p), dir.to_direction())]

    return Vector(
        -1 if next_dir == Direction.RIGHT else 1 if next_dir == Direction.LEFT else 0,
        -1 if next_dir == Direction.DOWN else 1 if next_dir == Direction.UP else 0,
    )


###############################################################################

current_point = Point(cave_row_bounds[0][0], 0)
current_dir = Vector(1, 0)
current_section = get_section(current_point)

all_points: Dict[Point, Vector] = {current_point: current_dir}


def execute_instruction(instr: Union[int, Direction]) -> None:
    global current_point
    global current_dir
    global current_section
    global all_points

    if isinstance(instr, Direction):
        current_dir = current_dir.rotate(instr)
        all_points[current_point] = current_dir
        return

    for _ in range(instr):
        next_point = current_point + current_dir
        next_dir = current_dir
        next_section = get_section(next_point)

        if next_section != current_section:
            next_point = get_entry_point(current_point, current_dir)
            next_dir = get_entry_dir(current_point, current_dir)
            next_section = get_section(next_point)

        if cave_map[next_point] == Tile.ROCK:
            break
        elif cave_map[next_point] == Tile.OPEN:
            current_point = next_point
            current_dir = next_dir
            current_section = next_section
            all_points[current_point] = current_dir
            continue


def print_map():
    for y in range(max_y + 1):
        for x in range(max_x + 1):
            p = Point(x, y)
            if p in all_points:
                d = all_points[p].to_direction()
                if d == Direction.UP:
                    print("^", end="")
                elif d == Direction.RIGHT:
                    print(">", end="")
                elif d == Direction.LEFT:
                    print("<", end="")
                elif d == Direction.DOWN:
                    print("v", end="")
                else:
                    assert False
                continue

            if p in cave_map:
                print(cave_map[p].value, end="")
                continue

            print(" ", end="")
        print()


for instr in instructions:
    execute_instruction(instr)
    # print_map()
    # print()
    # input()

print(current_point)
print(current_dir)

DIR_SCORE = {
    Vector(1, 0): 0,
    Vector(0, 1): 1,
    Vector(-1, 0): 2,
    Vector(0, -1): 3,
}

print(1000 * (current_point.y + 1) + 4 * (current_point.x + 1) + DIR_SCORE[current_dir])
