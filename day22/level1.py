from pathlib import Path
from dataclasses import dataclass
from typing import Union, Callable, Dict, List, Tuple
from enum import Enum, auto

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")


class Direction(Enum):
    RIGHT = "R"
    LEFT = "L"


@dataclass(frozen=True)
class Vector:
    x: int
    y: int

    def rotate_clockwise(self) -> "Vector":
        return Vector(-self.y, self.x)

    def rotate_counter_clockwise(self) -> "Vector":
        return Vector(self.y, -self.x)

    def rotate(self, dir: Direction) -> "Vector":
        if dir == Direction.LEFT:
            return self.rotate_counter_clockwise()
        elif dir == Direction.RIGHT:
            return self.rotate_clockwise()
        assert False


@dataclass(frozen=True)
class Point:
    x: int
    y: int

    def __add__(self, dir: Vector) -> "Point":
        return Point(self.x + dir.x, self.y + dir.y)


class Tile(Enum):
    ROCK = "#"
    OPEN = "."


cave_map: Dict[Point, Tile] = {}
instructions: List[Union[int, Direction]] = []

with INPUT_FILE.open("r") as f:
    lines = iter(f.readlines())
    for y, l in enumerate(lines):
        if len(l.strip()) == 0:
            break

        for x, c in enumerate(l.rstrip()):
            try:
                cave_map[Point(x, y)] = Tile(c)
            except ValueError:
                pass

    num_str = []
    for c in next(lines).strip():
        try:
            dir = Direction(c)
            instructions.append(int("".join(num_str)))
            instructions.append(dir)
            num_str = []
        except ValueError:
            num_str.append(c)
    if len(num_str) > 0:
        instructions.append(int("".join(num_str)))

max_x = max(p.x for p in cave_map.keys())
max_y = max(p.y for p in cave_map.keys())

cave_row_bounds: List[Tuple[int, int]] = [
    (
        min(p.x for p in cave_map.keys() if p.y == y),  #
        max(p.x for p in cave_map.keys() if p.y == y)
    ) for y in range(max_y + 1)
]
cave_col_bounds: List[Tuple[int, int]] = [
    (
        min(p.y for p in cave_map.keys() if p.x == x),  #
        max(p.y for p in cave_map.keys() if p.x == x)
    ) for x in range(max_x + 1)
]

###############################################################################

current_point = Point(cave_row_bounds[0][0], 0)
current_dir = Vector(1, 0)


def execute_instruction(instr: Union[int, Direction]) -> None:
    global current_point
    global current_dir

    if isinstance(instr, Direction):
        current_dir = current_dir.rotate(instr)
        return

    for _ in range(instr):
        next_point = current_point + current_dir

        if next_point not in cave_map:
            assert abs(current_dir.x) + abs(current_dir.y) == 1
            if current_dir.x > 0:
                next_point = Point(cave_row_bounds[next_point.y][0], next_point.y)
            elif current_dir.x < 0:
                next_point = Point(cave_row_bounds[next_point.y][1], next_point.y)
            elif current_dir.y > 0:
                next_point = Point(next_point.x, cave_col_bounds[next_point.x][0])
            elif current_dir.y < 0:
                next_point = Point(next_point.x, cave_col_bounds[next_point.x][1])

        if cave_map[next_point] == Tile.ROCK:
            break
        elif cave_map[next_point] == Tile.OPEN:
            current_point = next_point
            continue


for instr in instructions:
    execute_instruction(instr)
print(current_point)
print(current_dir)

DIR_SCORE = {
    Vector(1, 0): 0,
    Vector(0, 1): 1,
    Vector(-1, 0): 2,
    Vector(0, -1): 3,
}

print(1000 * (current_point.y + 1) + 4 * (current_point.x + 1) + DIR_SCORE[current_dir])
