from pathlib import Path
from collections import Counter

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")

UNIQUE_CHARS = 14

with INPUT_FILE.open("r") as f:
    for l in f.readlines():
        l = l.strip()
        chars = Counter(list(l[0:UNIQUE_CHARS]))

        i = UNIQUE_CHARS
        while len(chars) != UNIQUE_CHARS:
            chars.subtract(l[i - UNIQUE_CHARS])
            chars.update(l[i])
            i += 1
            chars = Counter({c: cnt for c, cnt in chars.items() if cnt > 0})

        print(i)
