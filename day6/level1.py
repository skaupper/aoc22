from pathlib import Path
from collections import Counter

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")

with INPUT_FILE.open("r") as f:
    for l in f.readlines():
        l = l.strip()
        chars = Counter(list(l[0:4]))

        i = 4
        while len(chars) != 4:
            chars.subtract(l[i - 4])
            chars.update(l[i])
            i += 1
            chars = Counter({c: cnt for c, cnt in chars.items() if cnt > 0})

        # print(l[i:i+4])
        print(i)
