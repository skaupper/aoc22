from pathlib import Path
from dataclasses import dataclass
from typing import Union, Callable, Dict, List, Tuple, Set
from enum import Enum, auto

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")
# INPUT_FILE = Path("example-2.txt")


@dataclass(frozen=True)
class Vector:
    x: int
    y: int


@dataclass(frozen=True)
class Point:
    x: int
    y: int

    def __add__(self, dir: Vector) -> "Point":
        return Point(self.x + dir.x, self.y + dir.y)


elf_positions: Set[Point] = set()

with INPUT_FILE.open("r") as f:
    for y, l in enumerate(f.readlines()):
        for x, c in enumerate(l.strip()):
            if c == "#":
                elf_positions.add(Point(x, y))

CHECKED_POINTS = [
    ((Vector(-1, -1), Vector(0, -1), Vector(1, -1)), Vector(0, -1)),  # NORTH
    ((Vector(-1, 1), Vector(0, 1), Vector(1, 1)), Vector(0, 1)),  # SOUTH
    ((Vector(-1, -1), Vector(-1, 0), Vector(-1, 1)), Vector(-1, 0)),  # WEST
    ((Vector(1, -1), Vector(1, 0), Vector(1, 1)), Vector(1, 0)),  # EAST
]
ENV_POINTS = [
    Vector(-1, -1),
    Vector(0, -1),
    Vector(1, -1),
    Vector(1, 0),
    Vector(1, 1),
    Vector(0, 1),
    Vector(-1, 1),
    Vector(-1, 0),
]


def do_step(positions: Set[Point], round_nr: int) -> Tuple[Set[Point], bool]:
    # Half turn #1
    elves_by_move: Dict[Point, List[Point]] = {}
    for p in positions:
        adjacent_points = set([p + m for m in ENV_POINTS])
        adjacent_elves = adjacent_points.intersection(positions)
        if len(adjacent_elves) == 0:
            continue

        for i in range(len(CHECKED_POINTS)):
            checks, move = CHECKED_POINTS[(round_nr+i) % len(CHECKED_POINTS)]
            checked_points = set([p + c for c in checks])

            if len(adjacent_elves.intersection(checked_points)) != 0:
                continue

            if p + move not in elves_by_move:
                elves_by_move[p + move] = []
            elves_by_move[p + move].append(p)
            break

    proposed_moves = {elves[0]: move for move, elves in elves_by_move.items() if len(elves) == 1}

    # Half turn #2
    return {proposed_moves.get(elf, elf) for elf in positions}, len(proposed_moves) == 0


def print_positions(positions: Set[Point]) -> None:
    min_x = min(p.x for p in positions)
    max_x = max(p.x for p in positions)
    min_y = min(p.y for p in positions)
    max_y = max(p.y for p in positions)

    for y in range(min_y, max_y + 1):
        for x in range(min_x, max_x + 1):
            if Point(x, y) in positions:
                print("#", end="")
            else:
                print(".", end="")
        print()


ROUND_CNT = 10

# Simulate rounds
# print_positions(elf_positions)
# print()
i = 0
halt = False
while not halt:
    if i % 10 == 0:
        print(i)
    elf_positions, halt = do_step(elf_positions, i)
    i += 1
    # print_positions(elf_positions)
    # print()

print(i)
