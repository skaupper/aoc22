from pathlib import Path
from dataclasses import dataclass
from typing import Union, Callable, Dict, List, Tuple, Set
from enum import Enum, auto

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")
# INPUT_FILE = Path("example-2.txt")


@dataclass(frozen=True)
class Vector:
    x: int
    y: int


@dataclass(frozen=True)
class Point:
    x: int
    y: int

    def __add__(self, dir: Vector) -> "Point":
        return Point(self.x + dir.x, self.y + dir.y)


elf_positions: Set[Point] = set()

with INPUT_FILE.open("r") as f:
    for y, l in enumerate(f.readlines()):
        for x, c in enumerate(l.strip()):
            if c == "#":
                elf_positions.add(Point(x, y))

CHECKED_POINTS = [
    ((Vector(-1, -1), Vector(0, -1), Vector(1, -1)), Vector(0, -1)),  # NORTH
    ((Vector(-1, 1), Vector(0, 1), Vector(1, 1)), Vector(0, 1)),  # SOUTH
    ((Vector(-1, -1), Vector(-1, 0), Vector(-1, 1)), Vector(-1, 0)),  # WEST
    ((Vector(1, -1), Vector(1, 0), Vector(1, 1)), Vector(1, 0)),  # EAST
]
ENV_POINTS = [
    Vector(-1, -1),
    Vector(0, -1),
    Vector(1, -1),
    Vector(1, 0),
    Vector(1, 1),
    Vector(0, 1),
    Vector(-1, 1),
    Vector(-1, 0),
]


def do_step(positions: Set[Point], round_nr: int) -> Set[Point]:
    # Half turn #1
    proposed_moves: Dict[Point, Point] = {}
    for p in positions:
        proposed_moves[p] = p
        if not any(p + m in positions for m in ENV_POINTS):
            continue

        for i in range(len(CHECKED_POINTS)):
            checks, move = CHECKED_POINTS[(round_nr+i) % len(CHECKED_POINTS)]

            chosen_move = None
            for c in checks:
                if p + c in positions:
                    break
            else:
                chosen_move = move
            if chosen_move is None:
                continue

            proposed_moves[p] = p + chosen_move
            break

    for curr_elf in positions:
        curr_move = proposed_moves[curr_elf]
        elves_with_same_moves = [
            elf for elf, elf_move in proposed_moves.items() if elf != curr_elf and elf_move == curr_move
        ]
        if len(elves_with_same_moves) == 0:
            continue

        proposed_moves[curr_elf] = curr_elf
        for elf in elves_with_same_moves:
            proposed_moves[elf] = elf

    # Half turn #2
    return {proposed_moves[elf] for elf in positions}


def print_positions(positions: Set[Point]) -> None:
    min_x = min(p.x for p in positions)
    max_x = max(p.x for p in positions)
    min_y = min(p.y for p in positions)
    max_y = max(p.y for p in positions)

    for y in range(min_y, max_y + 1):
        for x in range(min_x, max_x + 1):
            if Point(x, y) in positions:
                print("#", end="")
            else:
                print(".", end="")
        print()


ROUND_CNT = 10

# Simulate rounds
# print_positions(elf_positions)
# print()
for i in range(ROUND_CNT):
    elf_positions = do_step(elf_positions, i)
    # print_positions(elf_positions)
    # print()

# Calculate empty fields
min_x = min(p.x for p in elf_positions)
max_x = max(p.x for p in elf_positions)
min_y = min(p.y for p in elf_positions)
max_y = max(p.y for p in elf_positions)
print((max_x-min_x+1) * (max_y-min_y+1) - len(elf_positions))
