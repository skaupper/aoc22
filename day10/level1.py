from pathlib import Path
from typing import Tuple, List

INPUT_FILE = Path("input.txt")
INPUT_FILE = Path("example.txt")

with INPUT_FILE.open("r") as f:
    commands = [l.strip() for l in f.readlines()]


cycle_counter = 0
last_reg_x = 1
reg_x = 1

thresholds = [20,60,100,140,180,220]
signal_strengths: List[int]=[]

for c in commands:
    last_reg_x = reg_x

    match c.split(" "):
        case ["noop"]:
            cycle_counter+=1
        case ["addx", x]:
            cycle_counter+=2

    if len(thresholds) > 0:
        if cycle_counter > thresholds[0]:
            signal_strengths.append(last_reg_x*thresholds[0])
            thresholds = thresholds[1:]
        elif cycle_counter == thresholds[0]:
            signal_strengths.append(reg_x*thresholds[0])
            thresholds = thresholds[1:]

    match c.split(" "):
        case ["noop"]: pass
        case ["addx", x]: reg_x += int(x)



print(signal_strengths)
print(sum(signal_strengths))
