from pathlib import Path
from typing import Tuple, List, Iterable

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")

with INPUT_FILE.open("r") as f:
    commands = [l.strip() for l in f.readlines()]



class Interpreter:
    SCREEN_WIDTH = 40
    SCREEN_HEIGHT = 6

    def __init__(self, commands: Iterable[str]):
        self._commands = list(commands)
        self._cmd_idx: int = 0
        self._add_state: int = 0
        self._cycle_counter: int = 1
        self._reg_x: int = 1
        self._screen: List[List[bool]]= [[False for _ in range(self.SCREEN_WIDTH)] for _ in range(self.SCREEN_HEIGHT)]
        self._screen_pos: int = 0

    def step(self) -> None:
        self._draw_pixel()
        self._exec_cmd()

    def has_ended(self) -> bool:
        return self._cmd_idx >= len(self._commands)

    def get_current_cycle(self) -> int:
        return self._cycle_counter

    def get_current_reg_value(self) -> int:
        return self._reg_x

    def _exec_cmd(self) -> None:
        cmd = self._commands[self._cmd_idx]
        match cmd.split(" "):
            case ["noop"]:
                self._cycle_counter += 1
                self._cmd_idx += 1
            case ["addx", x]:
                if self._add_state == 1:
                    self._reg_x += int(x)
                    self._cmd_idx += 1
                    self._add_state = 0
                else:
                    self._add_state += 1
                self._cycle_counter += 1

    def _draw_pixel(self) -> None:
        lit = abs(self._screen_pos%self.SCREEN_WIDTH - self._reg_x%self.SCREEN_WIDTH) <= 1
        self._screen[self._screen_pos//self.SCREEN_WIDTH][self._screen_pos%self.SCREEN_WIDTH] = lit
        self._screen_pos+=1

    def draw_screen(self) -> None:
        for row in self._screen:
            print("".join(["#" if p else "." for p in row]))




interp = Interpreter(commands)

thresholds = [20,60,100,140,180,220]
strengths = []
while not interp.has_ended():
    interp.step()
    if interp.get_current_cycle() in thresholds:
        strengths.append(interp.get_current_cycle() * interp.get_current_reg_value())

print(strengths)
print(sum(strengths))


interp.draw_screen()
