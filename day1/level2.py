from pathlib import Path
from typing import List

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")

with INPUT_FILE.open("r") as f:
   elves: List[List[int]] = []
   curr_elf: List[int] = []
   for l in f.readlines():
      l = l.strip()
      if len(l) == 0:
         elves.append(curr_elf)
         curr_elf = []
         continue

      curr_elf.append(int(l))
   elves.append(curr_elf)

   elves.sort(key=lambda e: sum(e), reverse=True)

   print(sum([sum(e) for e in elves[0:3]]))
