import graphs
from pathlib import Path
from typing import List, Dict, FrozenSet
from dataclasses import dataclass
import re
import itertools

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")

INPUT_PATTERN = re.compile(r"Valve (\w+) has flow rate=(\d+); tunnels? leads? to valves? (\w+(?:, \w+)*)")


@dataclass(frozen=True)
class Valve:
    name: str
    flow_rate: int


def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return itertools.chain.from_iterable(itertools.combinations(s, r) for r in range(len(s) + 1))


# #################################################################################

neighbour_map: Dict[Valve, List[Valve]] = {}

with INPUT_FILE.open("r") as f:
    valves: Dict[str, Valve] = {}
    neighbour_map_raw: Dict[Valve, List[str]] = {}

    for l in f.readlines():
        m = INPUT_PATTERN.match(l.strip())
        assert m is not None
        valve = Valve(name=m.group(1), flow_rate=int(m.group(2)))
        valves[valve.name] = valve
        neighbour_map_raw[valve] = [v.strip() for v in m.group(3).split(",")]

    neighbour_map = {
        valve: [valves[n] for n in neighbours]  #
        for valve, neighbours in neighbour_map_raw.items()
    }

MAX_TIME = 26
START_VALVE = next(v for v in neighbour_map.keys() if v.name == "AA")
MAX_FLOW = sum(v.flow_rate for v in neighbour_map.keys())

# #################################################################################


class PathFinderProtocol(graphs.PathFinderProtocol[Valve]):

    def get_neighbours(self, point: Valve) -> List[graphs.Edge[Valve]]:
        return [graphs.Edge(n, 1) for n in neighbour_map[point]]


distance_map: Dict[Valve, Dict[Valve, int]] = {}
for v in neighbour_map:
    if v.flow_rate == 0 and v != START_VALVE:
        continue

    paths = graphs.get_all_shortest_paths(v, PathFinderProtocol())
    distance_map[v] = {
        dest_v: int(distance)  #
        for dest_v, (distance, _) in paths.items()  #
        if dest_v.flow_rate != 0
    }

ALL_UNOPENED_VALVES = frozenset([v for v in distance_map if v.flow_rate != 0])
cache = {}


def step(
    unopened_valves: FrozenSet[Valve],
    current_valve: Valve,
    time_left: int,
) -> int:
    args = (unopened_valves, current_valve, time_left)
    if args in cache:
        return cache[args]

    if time_left == 0 or len(unopened_valves) == 0:
        cache[args] = 0
        return 0

    max_pressure = 0
    for v in unopened_valves:
        distance = distance_map[current_valve][v]
        if distance + 1 > time_left:
            continue

        remtime = time_left - distance - 1
        max_pressure = max(
            max_pressure,
            step(unopened_valves.difference([v]), v, remtime) + v.flow_rate * remtime,
        )

    cache[args] = max_pressure
    return max_pressure


# #################################################################################

# Calculate maximum pressure values for all valve combinations
max_pressure_per_valve_comb = {}
for unopened_valves in powerset(ALL_UNOPENED_VALVES):
    s = frozenset(unopened_valves)
    max_pressure_per_valve_comb[s] = step(s, START_VALVE, MAX_TIME)

# Choose two subsets of all valves and check which combination releases the maximum amount of pressure
max_seq = 0
for valves_to_open_0 in max_pressure_per_valve_comb:
    for valves_to_open_1 in powerset(ALL_UNOPENED_VALVES.difference(valves_to_open_0)):
        max_seq = max(
            max_seq,
            max_pressure_per_valve_comb[valves_to_open_0] + max_pressure_per_valve_comb[frozenset(valves_to_open_1)],
        )
print(max_seq)
