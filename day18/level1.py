from pathlib import Path

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")

sides = set([])

with INPUT_FILE.open("r") as f:
    for l in f.readlines():
        x, y, z = map(float, l.strip().split(","))
        sides.symmetric_difference_update([
            (x + 0.5, y, z),
            (x - 0.5, y, z),
            (x, y + 0.5, z),
            (x, y - 0.5, z),
            (x, y, z + 0.5),
            (x, y, z - 0.5),
        ])

print(len(sides))
