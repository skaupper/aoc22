import graphs
from pathlib import Path
from dataclasses import dataclass
from typing import List, Set
import math

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")
# INPUT_FILE = Path("example-2.txt")


@dataclass(frozen=True)
class Point3:
    x: float
    y: float
    z: float


# Cubes from the original input
orig_cubes = set([])
# Track sides of cubes not shared with other cubes
sides = set([])
# Cubes which have sides in `sides` but are not contained in orig_cubes (i.e. potential air chambers)
cubes = set([])

with INPUT_FILE.open("r") as f:
    for l in f.readlines():
        x, y, z = map(float, l.strip().split(","))
        sides.symmetric_difference_update([
            Point3(x + 0.5, y, z),
            Point3(x - 0.5, y, z),
            Point3(x, y + 0.5, z),
            Point3(x, y - 0.5, z),
            Point3(x, y, z + 0.5),
            Point3(x, y, z - 0.5),
        ])
        orig_cubes.add(Point3(int(x), int(y), int(z)))

for s in sides:
    xs = [int(math.floor(s.x)), int(math.ceil(s.x))]
    ys = [int(math.floor(s.y)), int(math.ceil(s.y))]
    zs = [int(math.floor(s.z)), int(math.ceil(s.z))]
    cubes.add(Point3(xs[0], ys[0], zs[0]))
    cubes.add(Point3(xs[1], ys[1], zs[1]))
cubes.symmetric_difference_update(orig_cubes)


#
# Prepare for A*
#
class PathFinderProtocol(graphs.PathFinderProtocol[Point3]):

    def __init__(self, target: Point3, orig_cubes: Set[Point3]) -> None:
        self._orig_cubes = orig_cubes
        self._target = target

    def get_neighbours(self, point: Point3) -> List[graphs.Edge[Point3]]:
        possible_neighbours = [
            Point3(point.x + 1, point.y, point.z),
            Point3(point.x - 1, point.y, point.z),
            Point3(point.x, point.y + 1, point.z),
            Point3(point.x, point.y - 1, point.z),
            Point3(point.x, point.y, point.z + 1),
            Point3(point.x, point.y, point.z - 1),
        ]
        return [graphs.Edge(n, 1) for n in possible_neighbours if n not in self._orig_cubes]

    def get_distance_to_target(self, point: Point3) -> float:
        return abs(point.x - self._target.x) + abs(point.y - self._target.y) + abs(point.z - self._target.z)

    def is_target(self, point: Point3) -> bool:
        return point == self._target


#
# For every potential air chamber check if there is a way to the outside ((-1,-1,-1) is used as a definite outside location)
#
prot = PathFinderProtocol(Point3(-1, -1, -1), orig_cubes)
for c in cubes:
    path = graphs.get_shortest_path(c, prot)
    if path is not None:
        continue

    # If there is no path for the given cube to the outside, remove all its sides from the set ()
    for s in sides.intersection([
        Point3(c.x + 0.5, c.y, c.z),
        Point3(c.x - 0.5, c.y, c.z),
        Point3(c.x, c.y + 0.5, c.z),
        Point3(c.x, c.y - 0.5, c.z),
        Point3(c.x, c.y, c.z + 0.5),
        Point3(c.x, c.y, c.z - 0.5),
    ]):
        sides.remove(s)

print(len(sides))
