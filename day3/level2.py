from pathlib import Path

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")


def to_prio(t: str) -> int:
    assert len(t) == 1
    ascii = ord(t)
    if ord('a') <= ascii <= ord('z'):
        return ascii - ord('a') + 1
    elif ord('A') <= ascii <= ord('Z'):
        return ascii - ord('A') + 27
    assert False


with INPUT_FILE.open("r") as f:
    lines = [l.strip() for l in f.readlines()]

    total_prio = 0
    for group_idx in range(len(lines)//3):
        all_elves = [set(list(lines[group_idx*3+i])) for i in range(3)]
        badge_item = all_elves[0].intersection(
            all_elves[1].intersection(all_elves[2]))
        # print(badge_item)
        total_prio += sum(to_prio(i) for i in badge_item)

print(total_prio)
