from pathlib import Path

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")


def to_prio(t: str) -> int:
    assert len(t) == 1
    ascii = ord(t)
    if ord('a') <= ascii <= ord('z'):
        return ascii - ord('a') + 1
    elif ord('A') <= ascii <= ord('Z'):
        return ascii - ord('A') + 27
    assert False


with INPUT_FILE.open("r") as f:
    total_prio = 0
    for l in f.readlines():
        all_items = l.strip()
        shared_items = set(all_items[0:len(all_items)//2]).intersection(set(all_items[len(all_items)//2:]))
        # print(shared_items)
        total_prio += sum(to_prio(i) for i in shared_items)

print(total_prio)
