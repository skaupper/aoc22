from pathlib import Path
from typing import Tuple, List, Iterator, Callable, Union, Literal, Dict, Optional, Generator, Set, TypeVar, Iterable
from enum import Enum, auto
from dataclasses import dataclass
import re

INPUT_FILE = Path("input.txt")
START_X = 0
END_X = int(4e6)
# INPUT_FILE = Path("example.txt")
# START_X = 0
# END_X = 20

INPUT_PATTERN = re.compile(r"Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)")


@dataclass(frozen=True)
class Point:
    x: int
    y: int

    def get_distance(self, other: "Point") -> int:
        return abs(self.x - other.x) + abs(self.y - other.y)


@dataclass(frozen=True)
class Rectangle:
    left_top: Point
    right_bottom: Point

    def get_area(self) -> int:
        width = self.right_bottom.x - self.left_top.x + 1
        height = self.right_bottom.y - self.left_top.y + 1
        return width * height


@dataclass(frozen=True)
class Line:
    p1: Point
    p2: Point

    def __sub__(self, other: "Line") -> List["Line"]:
        assert self.p1.y == self.p2.y
        assert other.p1.y == other.p2.y

        assert self.p1.x <= self.p2.x
        assert other.p1.x <= other.p2.x

        if self.p1.y != other.p1.y or other.p1.x > self.p2.x or other.p2.x < self.p1.x:
            return [self]

        sub_lines = [
            Line(Point(self.p1.x, self.p1.y), Point(other.p1.x - 1, self.p2.y)),
            Line(Point(other.p2.x + 1, self.p1.y), Point(self.p2.x, self.p2.y)),
        ]

        return [l for l in sub_lines if l.p1.x <= l.p2.x]


sensors: List[Point] = []
beacons: List[Point] = []

with INPUT_FILE.open("r") as f:
    for l in iter(f.readlines()):
        m = INPUT_PATTERN.match(l.strip())
        assert m is not None

        s_x, s_y, b_x, b_y = m.groups()
        sensors.append(Point(int(s_x), int(s_y)))
        beacons.append(Point(int(b_x), int(b_y)))

T = TypeVar("T")


def flatten(lst: Iterable[Iterable[T]]) -> List[T]:
    return [ele for sub_lst in lst for ele in sub_lst]


def get_free_points_at_height(height: int) -> List[Line]:
    free_points = [Line(Point(START_X, height), Point(END_X, height))]

    for s_idx in range(len(sensors)):
        s = sensors[s_idx]
        distance = s.get_distance(beacons[s_idx])

        dy = abs(s.y - height)
        if dy > distance:
            continue

        s_line = Line(Point(s.x - (distance-dy), height), Point(s.x + (distance-dy), height))
        free_points = flatten(l - s_line for l in free_points)

    return free_points


all_free_points = []

for y in range(START_X, END_X + 1):
    if y % 100000 == 0:
        print(f"{y:7}: {len(all_free_points)}")
    all_free_points += get_free_points_at_height(y)

print(all_free_points)
print(sum(Rectangle(l.p1, l.p2).get_area() for l in all_free_points))

print(all_free_points[0].p1.x * int(4e6) + all_free_points[0].p1.y)
