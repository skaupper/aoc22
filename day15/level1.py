from pathlib import Path
from typing import Tuple, List, Iterator, Callable, Union, Literal, Dict, Optional, Generator, Set, TypeVar, Iterable
from enum import Enum, auto
from dataclasses import dataclass
import re

INPUT_FILE = Path("input.txt")
HEIGHT = 2000000
# INPUT_FILE = Path("example.txt")
# HEIGHT = 10

INPUT_PATTERN = re.compile(r"Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)")


@dataclass(frozen=True)
class Point:
    x: int
    y: int

    def get_distance(self, other: "Point") -> int:
        return abs(self.x - other.x) + abs(self.y - other.y)


sensors: List[Point] = []
beacons: List[Point] = []

with INPUT_FILE.open("r") as f:
    for l in iter(f.readlines()):
        m = INPUT_PATTERN.match(l.strip())
        assert m is not None

        s_x, s_y, b_x, b_y = m.groups()
        sensors.append(Point(int(s_x), int(s_y)))
        beacons.append(Point(int(b_x), int(b_y)))


def get_covered_points(sensor: Point, beacon: Point, height: int) -> Generator[Point, None, None]:
    d = sensor.get_distance(beacon)

    if sensor.y - d > height or sensor.y + d < height:
        return

    dy = abs(height - sensor.y)
    dx = d - dy

    for x in range(sensor.x - dx, sensor.x + dx + 1):
        yield Point(x, height)


T = TypeVar("T")


def flatten(lst: Iterable[Iterable[T]]) -> List[T]:
    return [ele for sub_lst in lst for ele in sub_lst]


covered_points: Set[Point] = set(
    flatten([get_covered_points(sensors[i], beacons[i], HEIGHT) for i in range(len(sensors))])
)

covered_points_y = [p for p in covered_points if p not in beacons and p not in sensors]

# print(list(sorted(list(covered_points_y), key=lambda p: p.x)))
print(len(covered_points_y))
