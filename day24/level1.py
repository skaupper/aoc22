from pathlib import Path
from dataclasses import dataclass
from typing import Union, Callable, Dict, List, Tuple, Set
from enum import Enum, auto
from graphs import PathFinderProtocol, Edge, get_all_shortest_paths, get_shortest_path
import numpy as np

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")
# INPUT_FILE = Path("example-2.txt")
# INPUT_FILE = Path("example-3.txt")


@dataclass(frozen=True)
class Vector:
    x: int
    y: int

    def __mul__(self, f: int) -> "Vector":
        return Vector(self.x * f, self.y * f)


@dataclass(frozen=True)
class Point:
    x: int
    y: int

    def __add__(self, dir: Vector) -> "Point":
        return Point(self.x + dir.x, self.y + dir.y)


blizzards: Set[Tuple[Point, Vector]] = set()

with INPUT_FILE.open("r") as f:
    lines = f.readlines()

    width = len(lines[0]) - 3
    height = len(lines[1:-1])

    start_point = Point(lines[0].index(".") - 1, -1)
    end_point = Point(lines[-1].index(".") - 1, height)

    for y, l in enumerate(lines[1:-1]):
        for x, c in enumerate(l.strip()):
            x -= 1
            if c == "<":
                blizzards.add((Point(x, y), Vector(-1, 0)))
            elif c == ">":
                blizzards.add((Point(x, y), Vector(1, 0)))
            elif c == "^":
                blizzards.add((Point(x, y), Vector(0, -1)))
            elif c == "v":
                blizzards.add((Point(x, y), Vector(0, 1)))

# ######################################################################################################################


@dataclass(frozen=True)
class Vertex:
    pos: Point
    step_idx: int


class PathFinderInst(PathFinderProtocol[Vertex]):

    def __init__(self, width: int, height: int, end_point: Point, blizzards: Set[Tuple[Point, Vector]]) -> None:
        self._width = width
        self._height = height
        self._blizzards = blizzards
        self._blizzard_period = np.lcm(width, height)
        self._end_point = end_point

    def get_neighbours(self, v: Vertex) -> List[Edge[Vertex]]:
        if v.pos == end_point:
            return []

        new_step_idx = (v.step_idx + 1) % self._blizzard_period
        candidates = [
            Vertex(v.pos + Vector(-1, 0), new_step_idx),
            Vertex(v.pos + Vector(1, 0), new_step_idx),
            Vertex(v.pos + Vector(0, -1), new_step_idx),
            Vertex(v.pos + Vector(0, 1), new_step_idx),
            Vertex(v.pos, new_step_idx),
        ]

        current_blizzards = set()
        for b in self._blizzards:
            curr_blizz_pos = b[0] + b[1] * new_step_idx
            curr_blizz_pos = Point(curr_blizz_pos.x % self._width, curr_blizz_pos.y % self._height)
            current_blizzards.add(curr_blizz_pos)

        filtered_candidates = [
            Edge(p, 1)  #
            for p in candidates
            if p.pos not in current_blizzards and \
                ((p.pos.x >= 0 and p.pos.x < self._width and \
                  p.pos.y >= 0 and p.pos.y < self._height) or \
                    p.pos == self._end_point or p.pos == v.pos)
        ]
        return filtered_candidates

    def get_distance_to_target(self, point: Vertex) -> float:
        return abs(point.pos.x - self._end_point.x) + abs(point.pos.y - self._end_point.y)

    def is_target(self, point: Vertex) -> bool:
        return point.pos == self._end_point


end_node = get_shortest_path(Vertex(start_point, 0), PathFinderInst(width, height, end_point, blizzards))
assert end_node is not None
print(len(end_node)-1)
