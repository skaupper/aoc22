from pathlib import Path
from typing import Tuple, List, Iterator, Callable, Union, Literal, Dict, Optional

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")

packets = [
    [[2]],
    [[6]],
]

with INPUT_FILE.open("r") as f:
    lines = iter(f.readlines())

    while True:
        try:
            l = next(lines).strip()
            if len(l) == 0:
                continue
            packets.append(eval(l))
        except StopIteration:
            break


def compare(lhs, rhs) -> int:
    if isinstance(lhs, int) and isinstance(rhs, int):
        if lhs == rhs:
            return 0
        elif lhs < rhs:
            return -1
        else:
            return 1

    if isinstance(lhs, list) and isinstance(rhs, list):
        for i in range(min(len(lhs), len(rhs))):
            res = compare(lhs[i], rhs[i])
            if res != 0:
                return res

        return compare(len(lhs), len(rhs))

    if isinstance(lhs, int):
        return compare([lhs], rhs)
    else:
        return compare(lhs, [rhs])


import functools

packets = sorted(packets, key=functools.cmp_to_key(compare))

idx_0 = packets.index([[2]]) + 1
idx_1 = packets.index([[6]]) + 1
print(idx_0 * idx_1)
