from pathlib import Path
from typing import Tuple, List, Iterator, Callable, Union, Literal, Dict, Optional

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")

pairs = []

with INPUT_FILE.open("r") as f:
    lines = iter(f.readlines())

    while True:
        try:
            pairs.append((eval(next(lines)), eval(next(lines))))
            assert len(next(lines).strip()) == 0
        except StopIteration:
            break


def compare(lhs, rhs) -> int:
    if isinstance(lhs, int) and isinstance(rhs, int):
        if lhs == rhs:
            return 0
        elif lhs < rhs:
            return -1
        else:
            return 1

    if isinstance(lhs, list) and isinstance(rhs, list):
        for i in range(min(len(lhs), len(rhs))):
            res = compare(lhs[i], rhs[i])
            if res != 0:
                return res

        return compare(len(lhs), len(rhs))

    if isinstance(lhs, int):
        return compare([lhs], rhs)
    else:
        return compare(lhs, [rhs])


sum = 0
for i, lists in enumerate(pairs):
    if compare(lists[0], lists[1]) <= 0:
        sum += i + 1

print(sum)
