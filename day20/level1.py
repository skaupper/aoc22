from pathlib import Path
from dataclasses import dataclass
from typing import Union, Callable, Dict, Optional, List, Generator

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")


@dataclass
class Node:
    value: int
    prev: Optional["Node"] = None
    next: Optional["Node"] = None

    def shift(self, amt: int) -> None:
        assert self.prev is not None and self.next is not None

        # Remove from list
        self.prev.next = self.next
        self.next.prev = self.prev

        # Search for the position to reinsert
        node = self.next
        if amt >= 0:
            for _ in range(abs(amt)):
                assert node.next is not None
                node = node.next
        else:
            for _ in range(abs(amt)):
                assert node.prev is not None
                node = node.prev

        # Insert node to list
        assert node.prev is not None
        self.next = node
        self.prev = node.prev
        node.prev = self
        self.prev.next = self


orig_values: List[Node] = []

with INPUT_FILE.open("r") as f:
    for l in f.readlines():
        orig_values.append(Node(int(l.strip())))

for i in range(len(orig_values)):
    orig_values[i - 1].next = orig_values[i]
    orig_values[i].prev = orig_values[i - 1]

for n in orig_values:
    n.shift(n.value)


def find_node_with_value(root: Node, value: int) -> Optional[Node]:
    if root.value == value:
        return root

    node = root
    while node.value != value:
        node = node.next
        assert node is not None
        if node == root:
            return None

    return node


def flatten(root: Node) -> Generator[Node, None, None]:
    yield root

    node = root.next
    while node != root:
        assert node is not None
        yield node
        node = node.next


node_0 = find_node_with_value(orig_values[0], 0)
assert node_0 is not None
all_nodes = list(flatten(node_0))

v_1000 = all_nodes[1000 % len(all_nodes)].value
v_2000 = all_nodes[2000 % len(all_nodes)].value
v_3000 = all_nodes[3000 % len(all_nodes)].value

print(v_1000 + v_2000 + v_3000)
