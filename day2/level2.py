from pathlib import Path

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")

SCORE_MAP = {
   "A": {
      "X": 3,
      "Y": 4,
      "Z": 8
   },
   "B": {
      "X": 1,
      "Y": 5,
      "Z": 9
   },
   "C": {
      "X": 2,
      "Y": 6,
      "Z": 7
   },
}

with INPUT_FILE.open("r") as f:
   total_score = 0
   for l in f.readlines():
      opponent, my = l.strip().split(" ", 1)
      total_score += SCORE_MAP[opponent][my]

print(total_score)
