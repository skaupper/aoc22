from pathlib import Path
from typing import Tuple, List, Iterator, Callable, Union, Literal, Dict, Optional
from enum import Enum, auto
from dataclasses import dataclass

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")


class MapEntryType(Enum):
    SAND = auto()
    AIR = auto()
    ROCK = auto()
    SAND_SOURCE = auto()


@dataclass(frozen=True)
class Point:
    x: int
    y: int


def fill_line(cave_map: Dict[Point, MapEntryType], p1: Point, p2: Point) -> None:
    dx = p2.x - p1.x
    dy = p2.y - p1.y

    if dx != 0:
        dx //= abs(dx)
    if dy != 0:
        dy //= abs(dy)

    p = p1
    while p != p2:
        cave_map[p] = MapEntryType.ROCK
        p = Point(p.x + dx, p.y + dy)
    cave_map[p] = MapEntryType.ROCK


def draw_map(cave_map: Dict[Point, MapEntryType]) -> None:
    left_top = Point(min(cave_map.keys(), key=lambda p: p.x).x, min(cave_map.keys(), key=lambda p: p.y).y)
    right_bottom = Point(max(cave_map.keys(), key=lambda p: p.x).x, max(cave_map.keys(), key=lambda p: p.y).y)

    for y in range(left_top.y, right_bottom.y + 1):
        print(f"{y:4}: ", end="")
        for x in range(left_top.x, right_bottom.x + 1):
            p = Point(x, y)

            entry: MapEntryType
            if p in cave_map:
                entry = cave_map[p]
            else:
                entry = MapEntryType.AIR

            if entry == MapEntryType.AIR:
                print(".", end="")
            elif entry == MapEntryType.ROCK:
                print("#", end="")
            elif entry == MapEntryType.SAND:
                print("o", end="")
            elif entry == MapEntryType.SAND_SOURCE:
                print("+", end="")
            else:
                assert False
        print()


def apply_sand(cave_map: Dict[Point, MapEntryType], src_point: Point) -> bool:
    bottom_y = max([p.y for p, t in cave_map.items() if t == MapEntryType.ROCK]) + 2

    p = src_point
    while True:
        under = Point(p.x, p.y + 1)
        under_left = Point(p.x - 1, p.y + 1)
        under_right = Point(p.x + 1, p.y + 1)

        if under not in cave_map and under.y < bottom_y:
            p = under
        elif under_left not in cave_map and under_left.y < bottom_y:
            p = under_left
        elif under_right not in cave_map and under_right.y < bottom_y:
            p = under_right
        else:
            cave_map[p] = MapEntryType.SAND
            return p != src_point


cave_map: Dict[Point, MapEntryType] = {}

with INPUT_FILE.open("r") as f:
    for l in f.readlines():
        points = l.split(" -> ")
        last_point = None

        for p_raw in points:
            p = Point(*[int(coord) for coord in p_raw.strip().split(",")])

            if last_point is not None:
                fill_line(cave_map, last_point, p)

            last_point = p

steps = 1
while apply_sand(cave_map, Point(500, 0)):
    steps += 1

draw_map(cave_map)
print(steps)
