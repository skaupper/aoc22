import functools
import enum
import functools
from pathlib import Path


INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")


tree_map = {}

with INPUT_FILE.open("r") as f:
    tree_map = [list(map(int, l.strip())) for l in f.readlines()]


height = len(tree_map)
width = len(tree_map[0])


class Direction(enum.Enum):
    UP = (0, -1)
    DOWN = (0, 1)
    LEFT = (-1, 0)
    RIGHT = (1, 0)


@functools.lru_cache(None)
def get_viewing_distance(x: int, y: int, direction: Direction) -> int:
    dx = direction.value[0]
    dy = direction.value[1]

    tree_height = tree_map[y][x]

    x += dx
    y += dy

    distance = 0

    while (0 <= x < width) and (0 <= y < height):
        distance += 1

        if tree_map[y][x] >= tree_height:
            break

        x += dx
        y += dy

    return distance


def get_scenic_score(x: int, y: int) -> int:
    return functools.reduce(lambda s1, s2: s1*s2, [
        get_viewing_distance(x, y, Direction.UP),
        get_viewing_distance(x, y, Direction.DOWN),
        get_viewing_distance(x, y, Direction.LEFT),
        get_viewing_distance(x, y, Direction.RIGHT),
    ], 1)


highest_score = 0

for y in range(height):
    for x in range(width):
        highest_score = max(highest_score, get_scenic_score(x, y))


print(highest_score)
