import functools
from pathlib import Path
from typing import Iterable, List, Tuple, Generator

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")


tree_map = {}

with INPUT_FILE.open("r") as f:
    tree_map = [list(map(int, l.strip())) for l in f.readlines()]


height = len(tree_map)
width = len(tree_map[0])

vertical_lines = [[(x, y) for y in range(height)] for x in range(width)]
horizontal_lines = [[(x, y) for x in range(width)] for y in range(height)]


def get_visible_trees(line: Iterable[Tuple[int, int]]) -> List[Tuple[int, int]]:
    curr_height = -1

    for tree in line:
        tree_height = tree_map[tree[1]][tree[0]]
        if tree_height > curr_height:
            yield tree
            curr_height = tree_height


all_visible_trees = functools.reduce(lambda s1, s2: s1.union(s2), [
    *[get_visible_trees(l) for l in vertical_lines],
    *[get_visible_trees(l) for l in horizontal_lines],
    *[get_visible_trees(reversed(l)) for l in vertical_lines],
    *[get_visible_trees(reversed(l)) for l in horizontal_lines]
], set())

# print(all_visible_trees)
print(len(all_visible_trees))
