from pathlib import Path
from typing import Dict, Optional, Iterator
from dataclasses import dataclass, field
import itertools

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")


class DirIter:
   base_dir: "Directory"
   base_dir_used: bool
   sub_dir_iter: Iterator["Directory"]

   def __init__(self, directory: "Directory"):
      self.base_dir = directory
      self.base_dir_used = False
      self.sub_dir_iter = iter(directory.sub_dirs.values())

   def __next__(self) -> "Directory":
      if not self.base_dir_used:
         self.base_dir_used = True
         return self.base_dir

      next_dir = next(self.sub_dir_iter)
      self.sub_dir_iter = itertools.chain(iter(next_dir.sub_dirs.values()), self.sub_dir_iter)
      return next_dir


@dataclass
class Directory:
   name: str
   parent: Optional["Directory"] = None
   files: Dict[str, int] = field(default_factory=dict)
   sub_dirs: Dict[str, "Directory"] = field(default_factory=dict)

   def __iter__(self) -> DirIter:
      return DirIter(self)

   def add_file(self, name: str, size: int) -> None:
      assert name not in self.files
      self.files[name] = size

   def add_directory(self, sub_dir: "Directory") -> None:
      assert sub_dir.name not in self.sub_dirs
      self.sub_dirs[sub_dir.name] = sub_dir

   def get_subdir(self, sub_dir_name: str) -> "Directory":
      assert sub_dir_name in self.sub_dirs
      return self.sub_dirs[sub_dir_name]

   def get_parent(self) -> "Directory":
      assert self.parent is not None
      return self.parent

   def print(self, indent: int = 0) -> None:
      print(f"{' ':{indent}}- {self.name} (dir)")
      indent += 2
      for f in self.files:
         print(f"{' ':{indent}}- {f} (file, size={self.files[f]})")
      for d in self.sub_dirs.values():
         d.print(indent)

   def get_size(self) -> int:
      file_size = sum(f_size for f_size in self.files.values())
      sub_dir_size = sum(sub_dir.get_size() for sub_dir in self.sub_dirs.values())
      return file_size + sub_dir_size


TOP = Directory("/")

with INPUT_FILE.open("r") as f:
   curr_dir = TOP
   curr_cmd = ""

   for l in f.readlines():
      l = l.strip()

      if l.startswith("$"):
         _, cmd = l.split(" ", 1)
         if cmd == "ls":
            curr_cmd = "ls"
            print(f"List files of directory '{curr_dir.name}'...")
         elif cmd.startswith("cd"):
            curr_cmd = "cd"
            _, dest_dir = cmd.split(" ", 1)

            if dest_dir == "..":
               curr_dir = curr_dir.get_parent()
            elif dest_dir == "/":
               curr_dir = TOP
            else:
               curr_dir = curr_dir.get_subdir(dest_dir)
            print(f"Change to directory '{curr_dir.name}'...")
         continue

      assert curr_cmd == "ls"
      size_or_dir, name = l.split(" ", 1)

      if size_or_dir == "dir":
         print(f"Add directory {name}...")
         curr_dir.add_directory(Directory(name, curr_dir))
      else:
         print(f"Add file {name}...")
         curr_dir.add_file(name, int(size_or_dir))

TOP.print()

print(sum(d.get_size() for d in TOP if d.get_size() <= 100000))
