from pathlib import Path
from dataclasses import dataclass
from typing import Union, Callable, Dict, Optional, List, Generator
import re
from enum import Enum

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")


@dataclass(frozen=True)
class Materials:
    ore: int = 0
    clay: int = 0
    obsidian: int = 0
    geode: int = 0

    def __sub__(self, other: "Materials") -> "Materials":
        return Materials(
            ore=self.ore - other.ore,
            clay=self.clay - other.clay,
            obsidian=self.obsidian - other.obsidian,
            geode=self.geode - other.geode,
        )

    def __add__(self, other: "Materials") -> "Materials":
        return Materials(
            ore=self.ore + other.ore,
            clay=self.clay + other.clay,
            obsidian=self.obsidian + other.obsidian,
            geode=self.geode + other.geode,
        )

    def cap(self, other: "Materials") -> "Materials":
        return Materials(
            ore=min(self.ore, other.ore),
            clay=min(self.clay, other.clay),
            obsidian=min(self.obsidian, other.obsidian),
            geode=self.geode + other.geode,
        )

    def __ge__(self, other: "Materials") -> bool:
        return  (self.ore       >= other.ore)       and \
                (self.clay      >= other.clay)      and \
                (self.obsidian  >= other.obsidian)  and \
                (self.geode     >= other.geode)


@dataclass(frozen=True)
class Recipes:
    ore_robot: Materials
    clay_robot: Materials
    obsidian_robot: Materials
    geode_robot: Materials


ORE_ROBOT_PATTERN = re.compile(r"Each ore robot costs (\d+) ore.")
CLAY_ROBOT_PATTERN = re.compile(r"Each clay robot costs (\d+) ore.")
OBSIDIAN_ROBOT_PATTERN = re.compile(r"Each obsidian robot costs (\d+) ore and (\d+) clay.")
GEODE_ROBOT_PATTERN = re.compile(r"Each geode robot costs (\d+) ore and (\d+) obsidian.")

recipes: List[Recipes] = []

with INPUT_FILE.open("r") as f:
    for l in f.readlines():
        l = l.strip()

        ore = ORE_ROBOT_PATTERN.search(l)
        clay = CLAY_ROBOT_PATTERN.search(l)
        obsidian = OBSIDIAN_ROBOT_PATTERN.search(l)
        geode = GEODE_ROBOT_PATTERN.search(l)
        assert ore is not None and clay is not None and obsidian is not None and geode is not None

        recipes.append(
            Recipes(
                ore_robot=Materials(ore=int(ore.group(1))),
                clay_robot=Materials(ore=int(clay.group(1))),
                obsidian_robot=Materials(ore=int(obsidian.group(1)), clay=int(obsidian.group(2))),
                geode_robot=Materials(ore=int(geode.group(1)), obsidian=int(geode.group(2))),
            )
        )

########################################################################################################################


@dataclass(frozen=True)
class RobotCount:
    ore_robot: int = 0
    clay_robot: int = 0
    obsidian_robot: int = 0
    geode_robot: int = 0

    def __add__(self, other: "RobotCount") -> "RobotCount":
        return RobotCount(
            ore_robot=self.ore_robot + other.ore_robot,
            clay_robot=self.clay_robot + other.clay_robot,
            obsidian_robot=self.obsidian_robot + other.obsidian_robot,
            geode_robot=self.geode_robot + other.geode_robot,
        )

    def generate_materials(self) -> Materials:
        return Materials(
            ore=self.ore_robot,
            clay=self.clay_robot,
            obsidian=self.obsidian_robot,
            geode=self.geode_robot,
        )


class RobotType(Enum):
    ORE = "ore"
    CLAY = "clay"
    OBSIDIAN = "obsidian"
    GEODE = "geode"


MAX_TIME = 32


def can_afford_robot(robot_type: RobotType, recipes: Recipes, materials: Materials) -> bool:
    if robot_type == RobotType.ORE:
        return materials >= recipes.ore_robot
    elif robot_type == RobotType.CLAY:
        return materials >= recipes.clay_robot
    elif robot_type == RobotType.OBSIDIAN:
        return materials >= recipes.obsidian_robot
    elif robot_type == RobotType.GEODE:
        return materials >= recipes.geode_robot
    assert False


cache = {}

MAX_SPEND_PER_ROUND = Materials(ore=5, clay=10, obsidian=20)


def get_max_geodes(
    recipes: Recipes,
    current_materials: Materials = Materials(),
    time_left: int = MAX_TIME,
    robot_count: RobotCount = RobotCount(ore_robot=1),
) -> int:

    # Exit condition: Out of time
    if time_left == 0:
        return 0

    args = (current_materials.cap(MAX_SPEND_PER_ROUND), time_left, robot_count)
    if args in cache:
        return cache[args]

    # Recursive calls
    new_materials = current_materials + robot_count.generate_materials()
    max_geodes = 0
    if can_afford_robot(RobotType.GEODE, recipes, current_materials):
        max_geodes = max(
            max_geodes,
            get_max_geodes(
                recipes,
                new_materials - recipes.geode_robot,
                time_left - 1,
                robot_count + RobotCount(geode_robot=1),
            ) + (time_left-1)
        )
    if can_afford_robot(
        RobotType.OBSIDIAN, recipes, current_materials
    ) and robot_count.obsidian_robot < MAX_SPEND_PER_ROUND.obsidian:
        max_geodes = max(
            max_geodes,
            get_max_geodes(
                recipes,
                new_materials - recipes.obsidian_robot,
                time_left - 1,
                robot_count + RobotCount(obsidian_robot=1),
            )
        )
    if can_afford_robot(
        RobotType.CLAY, recipes, current_materials
    ) and robot_count.clay_robot < MAX_SPEND_PER_ROUND.clay:
        max_geodes = max(
            max_geodes,
            get_max_geodes(
                recipes,
                new_materials - recipes.clay_robot,
                time_left - 1,
                robot_count + RobotCount(clay_robot=1),
            )
        )
    if can_afford_robot(RobotType.ORE, recipes, current_materials) and robot_count.ore_robot < MAX_SPEND_PER_ROUND.ore:
        max_geodes = max(
            max_geodes,
            get_max_geodes(
                recipes,
                new_materials - recipes.ore_robot,
                time_left - 1,
                robot_count + RobotCount(ore_robot=1),
            )
        )
    max_geodes = max(max_geodes, get_max_geodes(
        recipes,
        new_materials,
        time_left - 1,
        robot_count,
    ))

    cache[args] = max_geodes
    return max_geodes


max_geodes_by_blueprints = []

for i, r in enumerate(recipes[:3]):
    ROBOTS = [r.clay_robot, r.ore_robot, r.obsidian_robot, r.geode_robot]

    MAX_SPEND_PER_ROUND = Materials(
        ore=max(rob.ore for rob in ROBOTS) + 1,
        clay=max(rob.clay for rob in ROBOTS) + 1,
        obsidian=max(rob.obsidian for rob in ROBOTS) + 1,
    )
    cache = {}
    geodes = get_max_geodes(recipes[i])
    print(f"{i:2}: {geodes}")
    max_geodes_by_blueprints.append(geodes)

import functools

print(functools.reduce(lambda f1, f2: f1 * f2, max_geodes_by_blueprints, 1))
