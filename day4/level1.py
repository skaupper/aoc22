from pathlib import Path
from dataclasses import dataclass

INPUT_FILE = Path("input.txt")
# INPUT_FILE = Path("example.txt")

@dataclass
class Range:
    start: int
    end: int

    @classmethod
    def from_str(cls, range_str: str) -> "Range":
        start, end = [int(s) for s in range_str.strip().split("-", 1)]
        assert start <= end
        return cls(start, end)

    def fully_contains(self, other: "Range") -> bool:
        return other.start >= self.start and other.end <= self.end


with INPUT_FILE.open("r") as f:
    total = 0
    for l in f.readlines():
        elves = [Range.from_str(rng) for rng in l.strip().split(",")]
        if elves[0].fully_contains(elves[1]) or elves[1].fully_contains(elves[0]):
            total += 1

print(total)
